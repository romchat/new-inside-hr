import * as React from 'react';
import styles from './Home2Template.module.scss';
import { IHome2TemplateProps } from './IHome2TemplateProps';
import { escape } from '@microsoft/sp-lodash-subset';

import 'bootstrap/dist/css/bootstrap.css';
import 'jquery';
import 'popper.js';

import pnp from 'sp-pnp-js';

import { library } from '@fortawesome/fontawesome-svg-core';
import {  } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default class Home2Template extends React.Component<IHome2TemplateProps, {}> {
  public render(): React.ReactElement<IHome2TemplateProps> {
    return (
      <div className={ styles.home2Template }>

      </div>
    );
  }
}
