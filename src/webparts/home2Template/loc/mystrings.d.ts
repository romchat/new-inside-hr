declare interface IHome2TemplateWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'Home2TemplateWebPartStrings' {
  const strings: IHome2TemplateWebPartStrings;
  export = strings;
}
