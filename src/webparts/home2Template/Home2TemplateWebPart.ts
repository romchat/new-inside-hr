import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-webpart-base';

import * as strings from 'Home2TemplateWebPartStrings';
import Home2Template from './components/Home2Template';
import { IHome2TemplateProps } from './components/IHome2TemplateProps';

export interface IHome2TemplateWebPartProps {
  description: string;
}

export default class Home2TemplateWebPart extends BaseClientSideWebPart<IHome2TemplateWebPartProps> {

  public render(): void {
    const element: React.ReactElement<IHome2TemplateProps > = React.createElement(
      Home2Template,
      {
        description: this.properties.description
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
