import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-webpart-base';

import * as strings from 'GlobalNavWebPartStrings';
import GlobalNav from './components/GlobalNav';
import { IGlobalNavProps } from './components/IGlobalNavProps';

export interface IGlobalNavWebPartProps {
  description: string;
}

export default class GlobalNavWebPart extends BaseClientSideWebPart<IGlobalNavWebPartProps> {

  public render(): void {
    const element: React.ReactElement<IGlobalNavProps > = React.createElement(
      GlobalNav,
      {
        description: this.properties.description
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
