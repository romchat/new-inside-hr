declare interface IGlobalNavWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'GlobalNavWebPartStrings' {
  const strings: IGlobalNavWebPartStrings;
  export = strings;
}
