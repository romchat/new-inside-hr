import * as React from 'react';
import styles from './Temjai.module.scss';
import { ITemjaiProps } from './ITemjaiProps';
import { escape } from '@microsoft/sp-lodash-subset';

import 'bootstrap/dist/css/bootstrap.css';
import 'jquery';
import 'popper.js';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faTimes,faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default class Temjai extends React.Component<ITemjaiProps, {}> {
  public render(): React.ReactElement<ITemjaiProps> {
    library.add(faTimes,faPaperPlane);
    return (
      <div className={ styles.temjai }>
        <div className={styles.temjai+" "+styles.temjaitooltip+" "+styles.animation}>
          <input type="checkbox" id="temjaichk" className={styles.temjaichk}/>
          <label htmlFor="temjaichk" style={{cursor:'pointer'}} onClick={()=>{this.changeState()}}><img src={'https://betteritcorp.sharepoint.com/sites/test/HRInside/PublishingImages/temjais.png'} width="100%" height="100%"></img></label>
          <span id="tooltip1" className={styles.temjaitooltiptext}>พร้อมให้บริการตอบคำถามเกี่ยวกับ HR และ Admin แล้วค่ะ</span>
          <div id="tooltip2" className={styles.temjaitooltiptext2}>
            <div className="row no-gutters" style={{backgroundColor: 'steelblue', padding:'5px'}}>
                <div className="col no-gutters" style={{color:'white'}}><img src={'https://betteritcorp.sharepoint.com/sites/test/HRInside/PublishingImages/temjais.png'} width="30px" />น้องเต็มใจ</div>
                <div className="col no-gutters" style={{textAlign:'right'}} onClick={()=>{this.changeState()}}>
                    <label htmlFor="temjaichk" className={styles.closetemjai}>
                        <FontAwesomeIcon icon='times' />
                    </label>
                </div>
            </div>
            <div className={styles["message-block"]} id="msg">
              <div className={styles.tempjaiChat+" row no-gutters"}>
                  <div className={styles["icon-left"]}><img src={'https://betteritcorp.sharepoint.com/sites/test/HRInside/PublishingImages/temjais.png'} width="30px"/></div>
                  <div className="col">
                      <div className={styles.tempjaiText}>
                          สวัสดีค่ะ น้องเต็มใจยินดีให้บริการค่ะ
                          <br/>
                      </div>
                  </div>
              </div>
            </div>
            <div className="input-group input-message" style={{padding:'5px'}}>
                <input id="inputMsg" type="text" placeholder="Type something..." className="form-control" />
                <div className="input-group-append">
                    <button className="btn" type="button" id="sendMsg" style={{minWidth:"0"}}><FontAwesomeIcon icon="paper-plane" /></button>
                </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  private changeState(){
    let check = document.getElementById('temjaichk') as HTMLInputElement;
    console.log(check.checked);
    
    if(check.checked){
      document.getElementById('tooltip1').style.display = 'none';
      document.getElementById('tooltip2').style.display = 'block';
      document.getElementById('tooltip2').style.visibility = 'visible';
    }else{
      document.getElementById('tooltip1').style.display = 'block';
      document.getElementById('tooltip2').style.display = 'none';
      document.getElementById('tooltip2').style.visibility = 'hidden';
    }
  }
}
