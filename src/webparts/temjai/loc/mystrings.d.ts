declare interface ITemjaiWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'TemjaiWebPartStrings' {
  const strings: ITemjaiWebPartStrings;
  export = strings;
}
