export interface IState{
    postItem: any[];
    currentUser: any;
    allUser: any[];
    shareTitle: string;
    shareDescription: string;
    peopleSearch: any[];
    peopleSelect: any[];
}