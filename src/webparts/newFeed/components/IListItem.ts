export interface PostList{
    ID: number;
    Title: string;
    PostDescription: string;
    PostBy: string;
    PictureGroup: string;
}

export interface PictureImage{
    File: any;
}

export interface IUserEntityData {
    IsAltSecIdPresent: string;
    ObjectId: string;
    Title: string;
    Email: string;
    MobilePhone: string;
    OtherMails: string;
    Department: string;
  }
  
export interface IClientPeoplePickerSearchUser {
    Key: string;
    Description: string;
    DisplayText: string;
    EntityType: string;
    ProviderDisplayName: string;
    ProviderName: string;
    IsResolved: boolean;
    EntityData: IUserEntityData;
    MultipleMatches: any[];
}
