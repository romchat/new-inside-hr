import * as React from 'react';
import styles from './NewFeed.module.scss';
import { INewFeedProps } from './INewFeedProps';
import { IState } from './IState';
import { escape } from '@microsoft/sp-lodash-subset';
import { IClientPeoplePickerSearchUser } from './IListItem';
import { SPHttpClient, SPHttpClientResponse } from '@microsoft/sp-http';

import pnp from 'sp-pnp-js';
import 'bootstrap/dist/css/bootstrap.css';
import 'jquery';
import 'popper.js';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faThumbsDown, faThumbsUp, faReply, faShare, faCaretDown, faPaperPlane, faTimesCircle, faPaperclip } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default class NewFeed extends React.Component<INewFeedProps, IState> {
  constructor(props: INewFeedProps,state:IState){
    super(props);
    this.state ={
      postItem: [],
      currentUser: null,
      allUser: [],
      shareDescription: '',
      shareTitle: '',
      peopleSearch: [],
      peopleSelect: []
    }
    this.getCurrent();
    this.getPost();
  }
  public render(): React.ReactElement<INewFeedProps> {
    library.add(faThumbsDown,faThumbsUp,faReply,faShare,faCaretDown, faPaperPlane, faTimesCircle, faPaperclip)
    const postContent = this.state.postItem.map((item)=>{
      let user: any;
      let time = item.Created.split('T');
      this.state.allUser.forEach((u)=>{
        if(u.Id == item.AuthorId){
          user = u;
        }
      });
      console.log(user);
      if(user != undefined && item.ParentItemID == null){
        let likes;
        if(item.Like == null){
          likes = 0
        }else{
          likes = item.Like;
        }
        let allLiked = item.LikeUser;
        let likesBtn = <button className={styles.commentBtn} onClick={(e)=>{this.addLikes(item,e)}}><FontAwesomeIcon icon="thumbs-up"/> Like</button>;
        if(allLiked!=null){
          if(allLiked.indexOf(item.Id)){
            likesBtn = <button className={styles.commentBtn} onClick={(e)=>{this.removeLike(item,e)}}><FontAwesomeIcon icon="thumbs-down"/> Unlike</button>;
          }
        }
        let reply = this.getLastReply(item);
        if(user.Id == this.state.currentUser.Id){
          return(
            <div className="row no-gutters" style={{padding:'10px 0px'}}>
              <div className={styles.user}>
                <div className={styles.userIcon}>
                  <img src='/_layouts/15/userphoto.aspx?size=S&username=romchat@better-i-t.com' width="100%" height="100%"/>
                </div>
                <div style={{width:"100%",textAlign:'center'}}><span className="badge badge-success">Me</span> <span className="badge badge-info">Admin</span></div>
              </div>
              <div className={styles.myusertooltip}>
                <div className={styles.myusertooltiptext}>
                  <span className={styles.l}>{user.Title}</span><br/>
                  <span className={styles.xs}>{time[0]}</span>
                  <hr/>
                  <div className={styles.m+" "+styles.content} dangerouslySetInnerHTML={{__html:item.Body}}></div>
                </div>
              </div>
              <div className={styles.likeReplyShare}>
                <div>{likes} Likes :  |
                  {likesBtn} :
                  <button className={styles.commentBtn} onClick={(e)=>{this.toggleReplyBlock(item.ID,e)}}><FontAwesomeIcon icon="reply"/> Reply</button> : 
                  <button className={styles.commentBtn} onClick={(e)=>{this.toggleModal(e);}}><FontAwesomeIcon icon="share"/> Share</button>
                </div>
                <div id={"replyBlock"+item.ID} style={{display:'none'}}>
                  <div className="input-group input-message" style={{padding:'5px 0px'}}>
                      <input id="inputMsg" type="text" placeholder="Type something..." className="form-control" />
                      <div className="input-group-append">
                          <button className="btn" type="button" id="sendMsg"><FontAwesomeIcon icon="paper-plane" /></button>
                      </div>
                  </div>
                </div>
                <div>{reply}</div>
              </div>
            </div>
          );
        }else{
          return(
            <div className="row no-gutters" style={{padding:'10px 0px'}}>
              <div id="myModal" className={styles.mainModal}>
                {this.renderModal()}
              </div>
              <div id="requestModal" className={styles.mainModal}>
                {this.renderConfirmModal()}
              </div>
              <div id="finalModal" className={styles.mainModal}>
                {this.renderFinalModal()}
              </div>
              <div className={styles.user}>
                <div className={styles.userIcon}>
                  <img src={'/_layouts/15/userphoto.aspx?size=S&username='+user.Email} width="100%" height="100%"/>
                </div>
              </div>
              <div className={styles.usertooltip}>
                <div className={styles.usertooltiptext}>
                  <span className={styles.l}>{user.Title}</span><br/>
                  <span className={styles.xs}>{time[0]}</span>
                  <hr/>
                  <div className={styles.m+" "+styles.content} dangerouslySetInnerHTML={{__html:item.Body}}></div>
                </div>
              </div>
              <div className={styles.likeReplyShare}>
                <div>{likes} Likes :  |
                  {likesBtn} :
                   <button className={styles.commentBtn} onClick={(e)=>{this.toggleReplyBlock(item.ID,e)}}><FontAwesomeIcon icon="reply"/> Reply</button> : 
                   <button className={styles.commentBtn} onClick={(e)=>{this.toggleModal(e);}}><FontAwesomeIcon icon="share"/> Share</button>
                </div>
                <div id={"replyBlock"+item.ID} style={{display:'none'}}>
                  <div className="input-group input-message" style={{padding:'5px 0px'}}>
                      <input id="inputMsg" type="text" placeholder="Type something..." className="form-control" />
                      <div className="input-group-append">
                          <button className="btn" type="button" id="sendMsg" onClick={()=>{this.addReply(item.ID,user.Id,item)}}><FontAwesomeIcon icon="paper-plane" /></button>
                      </div>
                  </div>
                </div>
                <div id={"reply"+item.ID}></div>
              </div>
            </div>
          );
        }
      }
    });
    if(this.state.currentUser != null){
      return (
        <div className={ styles.newFeed } style={{paddingBottom:"20px"}}>
          <div id="myModal" className={styles.mainModal}>
            {this.renderModal()}
          </div>
          <div id="requestModal" className={styles.mainModal}>
            {this.renderConfirmModal()}
          </div>
          <div id="finalModal" className={styles.mainModal}>
            {this.renderFinalModal()}
          </div>
          <div className="container-fluid">
            <div className="row">
              <div className="col-3">
                <div>
                  <div className="row no-gutters" style={{backgroundColor:'white', margin:'20px',  padding:'20px'}}>
                    <div className={styles.userIcon} style={{marginRight:'20px'}}>
                      <img src='/_layouts/15/userphoto.aspx?size=S&username=romchat@better-i-t.com' width="100%" height="100%"/>
                    </div>
                    <div className="col no-gutters">
                      <div>
                        <a>{this.state.currentUser.Title}</a><br/>
                        <a>Department</a><br/>
                        <a>Edit Profile</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-9" style={{marginTop:'20px',backgroundColor:'white'}}>
                <div className="row" style={{marginRight:'20px',padding:'10px'}}>
                  <div className="col-12">
                    <ul className="nav s">
                      <li className={"nav-item "+styles.myNav3+" "+styles.animation}>
                          <a className="nav-link">All</a>
                      </li>
                      <li className={"nav-item "+styles.myNav3+" "+styles.animation}>
                          <a className="nav-link">Unread</a>
                      </li>
                      <li className={"nav-item "+styles.myNav3+" "+styles.animation}>
                          <a className="nav-link">@{this.state.currentUser.Title}</a>
                      </li>
                      <li className={"nav-item "+styles.myNav3+" "+styles.animation}>
                          <a className="nav-link">Replies</a>
                      </li>
                      <li className={"nav-item "+styles.myNav3+" "+styles.animation}>
                          <a className="nav-link">Show more <FontAwesomeIcon icon="caret-down"/></a>
                      </li>
                    </ul>
                  </div>
                </div>
                <hr style={{marginTop:'0'}}/>
                <div className='row' style={{marginBottom:'10px'}}>
                  <div className="col">
                    <div style={{width:'100%', padding:'5px', border:'1px solid gainsboro', borderRadius:'5px', marginBottom:"5px"}}>
                      <input type="text" id="textPost" placeholder="Post something..." style={{border:'0'}}></input>
                      <div id="postContent"></div>
                    </div>
                    <div style={{textAlign:'right'}}>
                      {/* <div>
                        <button type="button" style={{border:'0px', backgroundColor:'transparent'}}><FontAwesomeIcon icon="paperclip" /></button>
                      </div> */}
                      <button type="button" style={{backgroundColor:'steelblue', color:'white'}} onClick={()=>{this.addPost()}}>Post</button>
                    </div>
                  </div>
                </div>
                {postContent}
              </div>
            </div>
          </div>
          <div>
          </div>
        </div>
      );
    }else{
      return <div></div>;
    }
  }

  private addPost(){
    let temp = window.location.href.split('?');
    let title = atob(temp[1]);
    let textPost = document.getElementById('textPost') as HTMLInputElement;
    pnp.sp.web.lists.getByTitle(title).items.add({
      Title: this.state.currentUser.Title,
      Body: `<div class="ExternalClassC3383EAF217344ED90FB2ABCCD370775"><p>${textPost.value}</p></div>`
    }).then(()=>{
      this.getPost();
      textPost.value = '';
    });
  }

  private addReply(parent, parentEditor,item){
    let temp = window.location.href.split('?');
    let title = atob(temp[1]);
    let textReply = document.getElementById('inputMsg') as HTMLInputElement;
    pnp.sp.web.lists.getByTitle(title).items.add({
      ParentItemID: parent,
      ParentItemEditorId: parentEditor,
      Body: `<div class="ExternalClass697D83C680A64114B5B41735986CA039"><p>${textReply.value}</p></div>`
    }).then(()=>{
      this.getLastReply(item);
      textReply.value = '';
    });
  }

  private getLastReply(parent){
    let lastReply = this.state.postItem.map((item)=>{
      let user: any;
      let time = item.Created.split('T');
      this.state.allUser.forEach((u)=>{
        if(u.Id == item.AuthorId){
          user = u;
        }
      });
      let likes;
      if(item.Like == null){
        likes = 0
      }else{
        likes = item.Like;
      }
      let allLiked = item.LikeUser;
      let likesBtn = <button className={styles.commentBtn} onClick={(e)=>{this.addLikes(item,e)}}><FontAwesomeIcon icon="thumbs-up"/> Like</button>;
      if(allLiked!=null){
        if(allLiked.indexOf(item.Id)){
          likesBtn = <button className={styles.commentBtn} onClick={(e)=>{this.removeLike(item,e)}}><FontAwesomeIcon icon="thumbs-down"/> Unlike</button>;
        }
      }
      if(user != undefined){
        if(item.ParentItemID == parent.ID){
          return (
            <div style={{marginTop:'5px'}}>
              <div><img src={'/_layouts/15/userphoto.aspx?size=S&username='+user.Email} width="20px" height="20px"/> {user.Title}</div>
              <div dangerouslySetInnerHTML={{__html:item.Body}}></div>
            </div>
          );
        }
      }
    });
    return lastReply;
  }

  private renderFinalModal(): JSX.Element{ /* show reserve fail or success */
    let error = '';
    if(this.state.shareTitle == ''){
      error += ' Please input share title.';
    }
    if(this.state.peopleSelect.length==0){
      error += ' Please select people that you want to share this post.';
    }
    if(error!=''){
      return(
        <div className={styles.subModal}>
          <div style={{textAlign:'center', fontSize:'1.5em', fontWeight:800}}> {/* Header */}
            <div>
              <FontAwesomeIcon icon="times-circle" color="red" style={{width:'30px', height:'30px'}}/>
            </div>
            <b>Share fail</b>
            <div style={{fontSize:'0.8em', fontWeight:400}}>
              {error}
            </div>
          </div>
          <hr/>
          <div style={{textAlign:'right'}}> {/* Footer */}
            <button className="btn btn-danger" onClick={(e)=>{this.backtoinput(e);}} style={{marginRight:'10px'}}>Close</button>
          </div>
        </div>
      );
    }else{
      return(
        <div className={styles.subModal}>
          <div style={{textAlign:'center', fontSize:'1.5em', fontWeight:800}}> {/* Header */}
            <div>
              <FontAwesomeIcon icon="check-circle" color="green" style={{width:'30px', height:'30px'}}/>
            </div>
            <b>Share success</b>
          </div>
          <hr/>
          <div style={{textAlign:'right'}}> {/* Footer */}
            <button className="btn btn-success" onClick={(e)=>{this.closeFinal(e);}} style={{marginRight:'10px'}}>Confirm</button>
          </div>
        </div>
      );
    }
  }

  private closeFinal(e){ /* close final modal */
    e.preventDefault();
    document.getElementById('finalModal').style.opacity='0';
    document.getElementById('finalModal').style.zIndex='-1';  
    this.setState({
      peopleSearch: [],
      peopleSelect: [],
    });
  }

  private backtoinput(e){ /* close confirm modal and final modal when reserve fail */
    e.preventDefault();
    document.getElementById('requestModal').style.opacity='0';
    document.getElementById('requestModal').style.zIndex='-1';
    document.getElementById('finalModal').style.opacity='0';
    document.getElementById('finalModal').style.zIndex='-1';
    document.getElementById('myModal').style.opacity='1';
    document.getElementById('myModal').style.zIndex='300';
  }

  private renderConfirmModal(): JSX.Element{ /* confirm modal after you confirm your reserve */
    let selectedPeople: JSX.Element[] = this.state.peopleSelect.map((people)=>{
      let tempUrl = `https://betteritcorp.sharepoint.com/sites/HrInside/_layouts/15/userphoto.aspx?size=S&username=${people.EntityData.Email}`;
      return(
        <div className="row" style={{width:'200px', margin:'0px 15px 15px 15px', backgroundColor:'white', border:'1px solid #ced4da', borderRadius:'10px', padding:'10px'}}>
          <div className="col-3" style={{paddingLeft:'0'}}>
            <img src={tempUrl} width="100%" style={{borderRadius:'50%'}}/>
          </div>
          <div className="col-8" style={{padding:'0'}}>
            <div style={{display:'inline-block'}}>{people.DisplayText}</div>
          </div>
        </div>
      );
    });
    return(
      <div className={styles.subModal}>
        <div style={{textAlign:'center', fontSize:'1.5em', fontWeight:800}}> {/* Header */}
          <b>Please confirm this information</b>
        </div>
        <hr/>
        <div> {/* Content */}
          <form>
            <div className="form-group row">
              <label className="col-sm-4 col-form-label" style={{fontWeight:800}}>Title</label>
              <div className="col-sm-8">
                <input type="text" className="form-control-plaintext" id="title" value={this.state.shareTitle} readOnly/>
              </div>
            </div>
            <div className="form-group row">
              <label className="col-sm-4 col-form-label" style={{fontWeight:800}}>Description</label>
              <div className="col-sm-8">
                <input type="text" className="form-control-plaintext" id="description" value={this.state.shareDescription} readOnly/>
              </div>
            </div>
            <div className="form-group row">
              <label className="col col-form-label" style={{fontWeight:800}}>Share People</label>
            </div>
            <div className="row" style={{padding:'0px 20px', margin:'-15px -15px 0px'}}>
              {selectedPeople}
            </div>
          </form>
        </div>
        <hr />
        <div style={{textAlign:'right'}}> {/* Footer */}
          <button className="btn btn-success" onClick={(e)=>{this.confirm(e);}} style={{marginRight:'10px'}}>Confirm</button>
          <button className="btn btn-danger" onClick={(e)=>{this.backtoinput(e);}}>Cancel</button>
        </div>
      </div>
    );
  }

  private confirm(e){ /* close confirm open final modal */
    e.preventDefault();
    document.getElementById('finalModal').style.opacity='1';
    document.getElementById('finalModal').style.zIndex='500';
    document.getElementById('requestModal').style.opacity='0';
    document.getElementById('requestModal').style.zIndex='-1';
  }

  private toggleReplyBlock(ID,e){
    e.preventDefault();
    document.getElementById("replyBlock"+ID).style.display = 'block';
  }

  private addLikes(item,e){
    let temp = window.location.href.split('?');
    let title = atob(temp[1]);
      e.preventDefault();
      console.log(item);
      
      let liked = 0;
      if(item.Like != null){
        liked = item.Like;
      }
      let temps = '';
      if(item.LikeUser != null){
        temps = item.LikeUser;
      }
      let allLikes = item.LikeUser;
      let alreadyLikes = false;
      if(allLikes != null){
        let arr = allLikes.split(';');
        arr.forEach((user)=>{
          if(parseInt(user) == this.state.currentUser.Id){
            alreadyLikes = true
          }
        });
        if(alreadyLikes){
          temps += this.state.currentUser.Id+';';
        }
      }else{
        temps += this.state.currentUser.Id+';';
      }
      pnp.sp.web.lists.getByTitle(title).items.getById(item.Id).update({
        Like: liked+1,
        LikeUser: temps
      }).then(()=>{
        this.getPost();
      });
  }

  private getCurrent(){
    pnp.sp.web.currentUser.get().then((res)=>{
      this.setState({
        currentUser: res
      });
      pnp.sp.web.siteUsers.get().then((u)=>{
        this.setState({
          allUser: u
        });
      });
    },(err)=>{
      alert(err);
    });
  }

  private getPost(){
    let temp = window.location.href.split('?');
    let title = atob(temp[1]);
    pnp.sp.web.lists.getByTitle(title).items.orderBy('Created',false).get().then((res)=>{
      this.setState({
        postItem: res,
      });
    },(err)=>{
      alert(err);
    });
  }

  private removeLike(item,e){
    let temp = window.location.href.split('?');
    let title = atob(temp[1]);
    e.preventDefault();
    let liked = item.Like-1;
    let allLikes = item.LikeUser;
    let temps = '';
    console.log(item);
    if(allLikes != ''){
      let arr: any[] = allLikes.split(';');
      arr.forEach((user,index)=>{
        if(parseInt(user) != this.state.currentUser.Id){
          temps += user+";";
        }
      });
    }
    pnp.sp.web.lists.getByTitle(title).items.getById(item.Id).update({
      Like: liked,
      LikeUser: temps
    }).then(()=>{
      this.getPost();
    });
  }

  private toggleConfirmModal(e){ /* open confirm modal after click confirm on main modal */
    e.preventDefault();
    document.getElementById('requestModal').style.opacity='1';
    document.getElementById('requestModal').style.zIndex = '400';
    document.getElementById('myModal').style.opacity='0';
    document.getElementById('myModal').style.zIndex = '-1';
  }

  private toggleModal(e){ /* open main modal */
    e.preventDefault();
    document.getElementById('myModal').style.opacity='1';
    document.getElementById('myModal').style.zIndex = '300';
  }

  private closeModal(e){ /* close main modal */
    e.preventDefault();
    document.getElementById('myModal').style.opacity='0';
    document.getElementById('myModal').style.zIndex = '-1';
    this.setState({
      peopleSearch: [],
      peopleSelect: []
    });
  }


  private renderModal(){
    let searchOptions: JSX.Element[] = this.state.peopleSearch.map((people)=>{
      return (
        <li onMouseDown={((e)=>{this.addSelectedPeople(e,people);})}>{people.DisplayText}</li>
      );
    });

    let selectedPeople: JSX.Element[] = this.state.peopleSelect.map((people)=>{
      let tempUrl = `https://betteritcorp.sharepoint.com/sites/HrInside/_layouts/15/userphoto.aspx?size=S&username=${people.EntityData.Email}`;
      return(
        <div className="row" style={{width:'200px', margin:'0px 15px 15px 15px', backgroundColor:'white', border:'1px solid #ced4da', borderRadius:'10px', padding:'10px'}}>
          <div className="col-3" style={{paddingLeft:'0'}}>
            <img src={tempUrl} width="100%" style={{borderRadius:'50%'}}/>
          </div>
          <div className="col-8" style={{padding:'0'}}>
            <div style={{display:'inline-block'}}>{people.DisplayText}</div>
          </div>
          <div className="col-1" style={{padding:'0', cursor:'pointer'}} onClick={((e)=>{this.deleteSelectedPeople(e, people);})}>
            <FontAwesomeIcon icon="times-circle" color="red"/>
          </div>
        </div>
      );
    });

    return(
      <div className={styles.subModal}>
        <div style={{textAlign:'center', fontSize:'1.5em', fontWeight:800}}> {/* Header */}
          <b>Please insert this information</b>
        </div>
        <hr/>
        <div> {/* Content */}
          <form>
            <div className="form-group row">
              <label className="col-sm-4 col-form-label" style={{fontWeight:800}}>Title</label>
              <div className="col-sm-8">
                <input type="text" className="form-control" id="title" onChange={(e)=>{this.setShareTitle(e.target.value);}}/>
              </div>
            </div>
            <div className="form-group row">
              <label className="col-sm-4 col-form-label" style={{fontWeight:800}}>Description</label>
              <div className="col-sm-8">
                <input type="text" className="form-control" id="description" onChange={(e)=>{this.setShareDescription(e.target.value);}}/>
              </div>
            </div>
            <div className="form-group row">
              <label className="col-sm-4 col-form-label" style={{fontWeight:800}}>Search Share People</label>
              <div className="col-sm-8">
                <input type="text" className="form-control" id="reserving" placeholder="Type more than 2 character for search people" onChange={(e)=>{this.searchPeople(e.target.value);}} onBlur={((e)=>{e.target.value='', this.resetSearch(e);})}/>
                <div>
                  <ul className={styles.searchOptions}>
                    {searchOptions}
                  </ul>
                </div>
              </div>
            </div>
            <div className="row" style={{padding:'0px 20px', margin:'-15px -15px 0px'}}>
              {selectedPeople}
            </div>
          </form>
        </div>
        <hr />
        <div style={{textAlign:'right'}}> {/* Footer */}
          <button className="btn btn-success" onClick={(e)=>{this.toggleConfirmModal(e);}} style={{marginRight:'10px'}}>Confirm</button>
          <button className="btn btn-danger" onClick={(e)=>{this.closeModal(e);}}>Cancel</button>
        </div>
      </div>
    );
  }

    private searchPeople(text){ /* function search people */
    const userRequestUrl: string = `${this.props.siteUrl}/_api/SP.UI.ApplicationPages.ClientPeoplePickerWebServiceInterface.clientPeoplePickerSearchUser`;
    let principalType: number = 1;
    const userQueryParams = {
      'queryParams': {
        'AllowEmailAddresses': true,
        'AllowMultipleEntities': false,
        'AllUrlZones': false,
        'MaximumEntitySuggestions': 5,
        'PrincipalSource': 15,
        // PrincipalType controls the type of entities that are returned in the results.
        // Choices are All - 15, Distribution List - 2 , Security Groups - 4, SharePoint Groups - 8, User - 1.
        // These values can be combined (example: 13 is security + SP groups + users)
        'PrincipalType': principalType,
        'QueryString': text
      }
    };
    
    this.props.spHttpClient.post(userRequestUrl, SPHttpClient.configurations.v1,{body: JSON.stringify(userQueryParams)})
      .then((response: SPHttpClientResponse)=>{
        return response.json();
      }).then((response: {value:string})=>{
        let userQueryResult: IClientPeoplePickerSearchUser[] = JSON.parse(response.value);
        this.setState({
          peopleSearch: userQueryResult
        });
      });
  }

  private addSelectedPeople(e,people: IClientPeoplePickerSearchUser){ /* add people that you want */
    e.preventDefault();
    let temp = this.state.peopleSelect;
    let contain: boolean = false;
    temp.map((tempPeople)=>{
      if(tempPeople.EntityData.ObjectId == people.EntityData.ObjectId){
        contain = true;
      }
    });
    if(contain == false){
      temp.push(people);
      this.setState({
        peopleSelect: temp
      });
    }
    this.resetSearch(e);
  }

  private setShareTitle(text){
    let temp = text;
    this.setState({
      shareTitle: temp
    });
  }

  private resetSearch(e){ /* reset search value */
    e.preventDefault();
    document.getElementById('reserving').blur();
    this.setState({
      peopleSearch: []
    });
  }

  private setShareDescription(text){
    let temp = text;
    this.setState({
      shareDescription: temp
    });
  }

  private deleteSelectedPeople(e,people: IClientPeoplePickerSearchUser){ /* delete people that you selected */
    e.preventDefault();
    let temp = this.state.peopleSelect;
    for(var i =0;i<temp.length;i++){
      if(temp[i].EntityData.ObjectId == people.EntityData.ObjectId){
        temp.splice(i,1);
        this.setState({
          peopleSelect: temp
        });
      }
    }
  }
}
