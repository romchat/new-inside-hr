declare interface INewFeedWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'NewFeedWebPartStrings' {
  const strings: INewFeedWebPartStrings;
  export = strings;
}
