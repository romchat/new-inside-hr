declare interface ICreateGroupWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'CreateGroupWebPartStrings' {
  const strings: ICreateGroupWebPartStrings;
  export = strings;
}
