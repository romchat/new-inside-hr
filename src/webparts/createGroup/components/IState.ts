export interface IState{
    allGroup: any[];
    stepText: string;
    errorText: string[];
    createUrl: string;
    titleErr: boolean;
}