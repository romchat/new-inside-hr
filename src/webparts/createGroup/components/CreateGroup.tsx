import * as React from 'react';
import styles from './CreateGroup.module.scss';
import { ICreateGroupProps } from './ICreateGroupProps';
import { escape } from '@microsoft/sp-lodash-subset';
import { IState } from './IState';

import 'bootstrap/dist/css/bootstrap.css';
import 'jquery';
import 'popper.js';

import pnp from 'sp-pnp-js';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faPlusCircle, faTimes, faCamera, faTimesCircle, faCheckCircle, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const color = ['#eb4d4b','#6ab04c','#c7ecee','#4834d4','#f0932b','#badc58','#130f40','#22a6b3'];

export default class CreateGroup extends React.Component<ICreateGroupProps, IState> {
  constructor(props: ICreateGroupProps, state: IState){
    super(props);
    this.state={
      allGroup: [],
      stepText: '',
      errorText: [],
      createUrl: '',
      titleErr: false
    };
    this.getAllGroups();
  }
  public render(): React.ReactElement<ICreateGroupProps> {
    library.add(faPlusCircle, faTimes, faCheckCircle);
    let MainModal = this.renderMainModal();
    let resultModal = this.renderResultModal();
    let waitingModal = this.renderWaitingModal();
    let Groups = this.state.allGroup.map((item,index)=>{
      let title = item.Title as string;
      let image = (
        <div className={styles.cardImage} style={{backgroundColor: color[index]}}><div className={styles.centerDiv} style={{fontSize:'64px', color:'white'}}>{title.charAt(0).toUpperCase()}</div></div>
      );
      return (
        <div className={"col-md-3 "+styles.departmentCard} onClick={()=>{window.location.href = item.Url}}>
          <div className={styles.departmentCardInside}>
            {image}
            <div className={styles.cardBody}>
              <h4 className={styles.cardTitle}>{item.Title}</h4>
              <p className={styles.cardText}>{item.Description}</p>
            </div>
          </div>
        </div>
      );
    });
    return (
      <div className={ styles.createGroup }>
        <div className="container-fluid" style={{padding:'0'}}>
          <div className={styles.headerContent} style={{backgroundImage:`url('${this.props.siteUrl}/PublishingImages/banner.jpg')`, backgroundRepeat:'no-repeat',backgroundSize:'100% 100%'}}>
            <div className={styles.headerContentInner}>
              <h1 className={styles.homeTitle}>NEW HR INSIDE</h1>
              <hr />
              <p className={styles.hrHeader}>Description for new hr inside</p>
              <button className="btn btn-primary">Find more about</button>
            </div>
          </div>
          <div className={styles.departmentBlock}>
            <div className={styles.departmentContent}>
              <h4 className={styles.departmentTitle}>HR Inside Group</h4>
              <br/>
              <br/>
              <div className="row">
                {Groups}
                <div className={styles.departmentCard}>
                  <div className={styles.departmentCardInside}>
                    <div className={styles.addDepartmentDiv} onClick={()=>{this.toggleCreateModal();}}>
                      <FontAwesomeIcon icon="plus-circle" color="#007bff" size="2x"/>
                      <p style={{color:"#007bff"}}>Add New Department</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="backgroundModal" className={styles.backgroundModal}></div>
        <div id="mainModal" className={styles.mainModal}>{MainModal}</div>
        <div id="waitingModal" className={styles.mainModal}>{waitingModal}</div>
        <div id="resultModal" className={styles.mainModal}>{resultModal}</div>
      </div>
    );
  }

  private renderWaitingModal(){
    return(
      <div>
        <div className={styles.bouncing}>
          <div className={styles.loader}>
            <span></span>
            <span></span>
            <span></span>
          </div>
          <div className={styles.textLoader}>
            <div>Please Waiting...</div>
            <div>This action should not take long.</div>
          </div>
        </div>
      </div>
    );
  }

  private renderResultModal(){
    return(
      <div>
        <div className={styles.modalTitle} style={{textAlign:'center'}}> {/* Header */}
          <div>
            <FontAwesomeIcon icon="check-circle" color="green" style={{width:'100px', height:'100px'}}/>
          </div>
          <b><h3>{this.state.stepText}</h3></b><br/>
          <h5>Url: <a href={`${this.props.siteUrl}/Pages/Home1.aspx?${this.state.createUrl}`}>{this.props.siteUrl}/Pages/Home1.aspx?{this.state.createUrl}</a></h5>
        </div>
        <div className={styles.modalContent} style={{textAlign:'right'}}> {/* Footer */}
          <button className="btn btn-secondary" type="button" onClick={()=>{this.closeModal();}} style={{marginRight:'10px'}}>Close</button>
        </div>
      </div>
    );
  }

  private toggleWaitingModal(){
    document.getElementById('waitingModal').style.transform = 'scale(1)';
    document.getElementById('mainModal').style.zIndex = '100';
  }

  private toggleResultModal(){
    document.getElementById('waitingModal').style.transform = 'scale(0)';
    document.getElementById('resultModal').style.transform = 'scale(1)';
    document.getElementById('backgroundModal').style.display = 'block';
    document.getElementById('mainModal').style.transform = 'scale(0)';
  }

  private checkError(){
    let inputTitle = document.getElementById('title') as HTMLInputElement;
    if(inputTitle.value == ''){
      this.setState({
        titleErr: true
      });
    }else{
      this.setState({
        titleErr: false
      });
    }
  }

  private renderMainModal(){
    let titleErr = <div></div>;
    let titleBorder = '1px solid #ced4da';
    if(this.state.titleErr){
      titleErr = <div style={{color:'red'}}>Please insert your group title</div>;
      titleBorder = '1px solid red';
    }
    return (
      <div>
        <div className="row" style={{margin:'0'}}>
          <div className="col" style={{padding:'0'}}>
            <text className={styles.modalTitle}>Please Input Group Information</text>
          </div>
          <div style={{cursor:'pointer'}} onClick={()=>{this.closeModal();}}>
            <FontAwesomeIcon icon="times" color="gainsboro" />
          </div>
        </div>
        <hr />
        <form className={styles.modalContent} id="form">
          <div className="form-group row">
            <label className="col-md-2 col-form-label"><b>Title <a style={{color:'red'}}>*</a></b></label>
            <div className="col-md-10">
              <input type="text" id="title" className="form-control" style={{border: titleBorder}} onChange={()=>{this.checkError();}}/>
              {titleErr}
            </div>
          </div>
          <div className="form-group row">
            <label className="col-md-2 col-form-label"><b>Description</b></label>
            <div className="col-md-10">
              <input type="text" id="description" className="form-control"/>
            </div>
          </div>
          <div className="form-group row">
            <label className="col-md-2 col-form-label"><b>Group Privacy</b></label>
            <div className="col-md-10">
              <select id="privacy" className="form-control" defaultValue="Private">
                <option>Private</option>
                <option>Public</option>
              </select>
            </div>
          </div>
        </form>
        <div style={{textAlign:'right'}}> {/* Footer */}
          <button className={"btn btn-success " +styles.modalContent} type="button" style={{marginRight:'10px'}} onClick={()=>{this.createGroups();}}>Create</button>
          <button className={"btn btn-secondary " +styles.modalContent} type="button" onClick={()=>{this.closeModal();}}>Cancel</button>
        </div>
      </div>
    );
  }

  private closeModal(){
    let inputTitle = document.getElementById('title') as HTMLInputElement;
    inputTitle.value = '';
    let inputDes = document.getElementById('description') as HTMLInputElement;
    inputDes.value = '';
    this.setState({
      stepText: '',
      errorText: [],
      createUrl: '',
    });
    document.getElementById('resultModal').style.transform = 'scale(0)';
    document.getElementById('backgroundModal').style.display = 'none';
    document.getElementById('mainModal').style.transform = 'scale(0)';
  }

  private toggleCreateModal(){
    document.getElementById('backgroundModal').style.display = 'block';
    document.getElementById('backgroundModal').style.zIndex = '200';
    document.getElementById('mainModal').style.transform = 'scale(1)';
    document.getElementById('mainModal').style.zIndex = '300';
  }


  private getAllGroups(){
    pnp.sp.web.lists.getByTitle('All Group').items.get().then((res)=>{
      this.setState({
        allGroup: res
      });
    });
  }

  private createGroups(){
    let inputTitle = document.getElementById('title') as HTMLInputElement;
    let inputDes = document.getElementById('description') as HTMLInputElement;
    let inputPrivacy = document.getElementById('privacy') as HTMLInputElement;
    let base64Url = btoa(inputTitle.value);
    if(inputTitle.value != ''){
      this.toggleWaitingModal();
      pnp.sp.web.lists.getByTitle('All Group').items.add({
        Title: inputTitle.value,
        Description: inputDes.value,
        Privacy: inputPrivacy.value,
        Url: this.props.siteUrl+'/Pages/Home1.aspx?'+base64Url
      }).then((res)=>{
        setTimeout(() => {
          this.setState({
            stepText: `Create ${inputTitle.value} group successfully.`,
            createUrl: base64Url
          });
          this.getAllGroups();
          this.toggleResultModal(); 
        }, (5000));
      });
    }
  }
}
