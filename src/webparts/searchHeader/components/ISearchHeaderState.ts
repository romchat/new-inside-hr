export interface ISearchHeaderState{
    searchFilter: string;
    searchFilterItem: any[];
    placeholderTxt: string;

    grouptitle: string;
}