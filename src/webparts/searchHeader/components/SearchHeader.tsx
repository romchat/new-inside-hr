import * as React from 'react';
import styles from './SearchHeader.module.scss';
import { ISearchHeaderProps } from './ISearchHeaderProps';
import { ISearchHeaderState } from './ISearchHeaderState';
import { escape } from '@microsoft/sp-lodash-subset';
import pnp from 'sp-pnp-js';
import 'bootstrap/dist/css/bootstrap.css';
import 'jquery';
import 'popper.js';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faSearch, faCog, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

let tmpFilter = '';
export default class SearchHeader extends React.Component<ISearchHeaderProps, ISearchHeaderState> {
  constructor(props:ISearchHeaderProps, state:ISearchHeaderState){
    super(props);
    let temp = window.location.href.split('?');
    let title = atob(temp[1])
    this.state = {
      searchFilter: 'All',
      searchFilterItem: [],
      placeholderTxt: 'Search for all...',
      grouptitle: title.toUpperCase()
    };
  }
  public render(): React.ReactElement<ISearchHeaderProps> {
    library.add(faSearch, faCog, faEnvelope);
    return (
      <div className={ styles.searchHeader }>
        <div className="container-fluid">
          <div className={"row"} style={{padding:'10px', paddingBottom:0, backgroundColor:'white'}}>
              <div className={""} style={{padding:'5px 10px'}}>
                  <img src={this.props.siteUrl+'/PublishingImages/logo.png'} height="50px"/>
              </div>
              <div className={"col"} style={{fontSize:"36px", padding:'5px 10px'}}>
                {this.state.grouptitle}
              </div>
              <div className={"col-lg-4"}>
                  <div className={"row"} style={{padding:'10px'}}>
                      <div className={"input-group mb-3"}>
                          <div className={"input-group-prepend"}>
                              <button id="dropBtn" type="button" className={"btn btn-outline-primary dropdown-toggle "+ styles.s} style={{minWidth:'0'}} onClick={()=>{this.toggleDropdown();}} onBlur={()=>{this.hideDropdown();this.changeFilter();}}>{this.state.searchFilter}</button>
                              <div className={"dropdown-menu"} id="dropdownMenu" style={{display:'none'}}>
                                <a className={'dropdown-item'} href="#" onMouseOver={()=>{this.setDropdownTemp('All');}}>All</a>
                                <a className={'dropdown-item'} href="#" onMouseOver={()=>{this.setDropdownTemp('This Site');}}>This Site</a>
                                <a className={'dropdown-item'} href="#" onMouseOver={()=>{this.setDropdownTemp('This Group');}}>This Group</a>
                              </div>
                          </div>
                          <input id="inputSearch" type="text" className={"form-control "+styles.s} placeholder={this.state.placeholderTxt}/>
                          <div className={"input-group-append"}>
                              <button className={"btn btn-outline-primary "+styles.s} type="button" id="button-addon2" style={{minWidth:'0'}}><FontAwesomeIcon icon={'search'}/></button>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
        <div className="container-fluid" style={{backgroundColor:'#e8e9e9'}}>
            <div className="row"style={{backgroundColor:'#e8e9e9'}}>
                <div className="col">
                    <ul className="nav justify-content-end s">
                        <li className={"nav-item "+styles.myNav2}>
                            <a className="nav-link" style={{color:'#313939',cursor:'pointer'}}><FontAwesomeIcon icon="cog" color="#313939" /> Settings</a>
                        </li>
                        <li className={"nav-item "+styles.myNav2}>
                            <a className="nav-link" style={{color:'#313939',cursor:'pointer'}}><FontAwesomeIcon icon="envelope" color="#313939" /> Invite</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
      </div>
    );
  }

  private toggleDropdown(){
    let item = document.getElementById('dropdownMenu');
    item.style.display = 'block';
  }

  private hideDropdown(){
    let item = document.getElementById('dropdownMenu');
    item.style.display = 'none';
  }
  private setDropdownTemp(tItem){
      tmpFilter = tItem;
      console.log(tmpFilter);
  }

  private changeFilter(){
    this.setState({
      searchFilter: tmpFilter,
      placeholderTxt: 'Search for '+tmpFilter.toLowerCase()+'...'
    });
  }
}
