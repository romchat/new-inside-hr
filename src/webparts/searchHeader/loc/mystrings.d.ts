declare interface ISearchHeaderWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'SearchHeaderWebPartStrings' {
  const strings: ISearchHeaderWebPartStrings;
  export = strings;
}
