import * as React from 'react';
import styles from './Home1Template.module.scss';
import { IHome1TemplateProps } from './IHome1TemplateProps';
import { escape, times } from '@microsoft/sp-lodash-subset';
import { IState } from './IState';
import { IClientPeoplePickerSearchUser } from '../../newFeed/components/IListItem';
import { SPHttpClient, SPHttpClientResponse } from '@microsoft/sp-http';

import 'bootstrap/dist/css/bootstrap.css';
import 'jquery';
import 'popper.js';

import pnp from 'sp-pnp-js';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faTimes, faCalendarAlt, faFileAlt, faVideo, faPlus, faMinus, faThumbsUp, faThumbsDown, faReply, faShare, faCaretDown, faPaperPlane, faTimesCircle, faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

let documentList = 'TemplateFloder';
let eventList = 'TemplateEvents';
let dicussionList = 'G1';

export default class Home1Template extends React.Component<IHome1TemplateProps, IState> {
  constructor(props:IHome1TemplateProps, state: IState){
    super(props);
    let today = new Date();
    let nextMonth;
    let previousMonth;
    let nextYear;
    let previousYear;
    if(today.getMonth()==11){
      nextMonth = 0;
      nextYear = today.getFullYear()+1;
      previousMonth = today.getMonth()-1;
      previousYear = today.getFullYear();
    }else if(today.getMonth()==0){
      previousMonth = 11;
      previousYear = today.getFullYear()-1;
      nextMonth = today.getMonth()+1;
      nextYear = today.getFullYear();
    }else{
      nextMonth = today.getMonth()+1;
      previousMonth = today.getMonth()-1;
      nextYear = today.getFullYear();
      previousYear = today.getFullYear();
    }

    this.state = {
      documentItems: [],
      selectedDocument: null,
      selectNo: '',
      fileSelect: [],

      selectedEvent: null,
      relatedEvent: [],
      nextMonthEvents: [],
      previousMonthEvents: [],
      thisMonthEvents: [],
      thisMonth: today.getMonth(),
      nextMonth: nextMonth,
      thisYear: today.getFullYear(),
      previousMonth: previousMonth,
      nextYear: nextYear,
      previousYear: previousYear,

      discussionItem: [],
      startFolder: 'Documents',
      previousFolder: ['Documents'],
      modalContent: '',

      postItem: [],
      currentUser: null,
      allUser: [],
      shareDescription: '',
      shareTitle: '',
      peopleSearch: [],
      peopleSelect: []
    }
    this.getDocuments();
    this.getEvents(previousMonth+1,previousYear,nextMonth+1,nextYear,today.getMonth()+1,today.getFullYear());
  }
  public render(): React.ReactElement<IHome1TemplateProps> {
    library.add(faTimes, faCalendarAlt, faFileAlt, faVideo, faPlus, faMinus, faThumbsUp, faThumbsDown, faReply, faShare, faCaretDown, faPaperPlane, faTimesCircle, faPlusCircle);
    let MainModal = this.renderModalContent();
    let Doc = this.renderDoc();
    let prevFolder = this.state.previousFolder.map((fol,index)=>{
      if(index == this.state.previousFolder.length-1){
        return (<div><button type="button" className={styles.folderBtn} onClick={()=>{this.backFolder(index,fol)}}>{fol}</button></div>);
      }else{
        return (<div><button type="button" className={styles.folderBtn} onClick={()=>{this.backFolder(index,fol)}}>{fol}</button>/</div>);
      }
    });

    let eve = this.renderEvents();

    let vid = this.renderVideo();

    return (
      <div className={ styles.home1Template }>
        <div className="container-fluid">
          <div className="row">

            <div className="col-xl-4">
              <div className={styles.headerBlockTitle}>Documents</div>
              <div className={styles.contentBlock}>
                <div className="row no-gutters" style={{padding:"10px"}}>{prevFolder}</div>
                <hr style={{margin:"0px 20px"}} />
                {Doc}
                {/* <div style={{textAlign:'right', padding:'5px 10px',color:'grey', cursor:'pointer'}}><button type="button" className={styles.folderBtn} onClick={()=>{this.toggleUpload()}}>Upload document</button></div> */}
              </div>
            </div>

            <div className="col-xl-4">
              <div className={styles.headerBlockTitle}>New Videos</div>
              <div className={styles.contentBlock}>
                {vid}
                <div style={{textAlign:'right', padding:'5px 10px',color:'grey', cursor:'pointer'}}><button type="button" className={styles.folderBtn}>More videos...</button></div>
              </div>
            </div>

            <div className="col-xl-4">
              <div className={styles.headerBlockTitle}>Upcoming Events</div>
              <div className={styles.contentBlock}>
                {eve}
                <div style={{textAlign:'right', padding:'5px 10px',color:'grey', cursor:"pointer"}}><button type="button" className={styles.folderBtn}>See calendar...</button></div>
              </div>
            </div>
          </div>
        </div>
        <div id="backgroundModal" className={styles.backgroundModal}></div>
        <div id="mainModal" className={styles.mainModal}>{MainModal}</div>
      </div>
    );
  }

  private renderDoc(){
    if(this.state.documentItems.length != 0){
      let doc = this.state.documentItems.filter((item)=>{
        if(item.FileRef.split('/')[item.FileRef.split('/').length-2] == this.state.startFolder){
          return item;
        }
      });
      let returnItem = doc.map((item)=>{
        if(item.File_x0020_Type != null){
          return (
            <div className={"row no-gutters "+styles.docBlock} style={{padding:'10px 40px',cursor:'pointer'}} onClick={()=>{this.toggleDocumentModal(item)}}><img src={this.props.siteUrl+'/PublishingImages/documentIcon/'+item.File_x0020_Type+'Icon.png'} width="30px" style={{paddingRight:"10px"}} /> {item.File.Name}</div>
          );
        }else{
          return (
            <div className={"row no-gutters "+styles.docBlock} style={{padding:'10px 40px',cursor:'pointer'}} onClick={()=>{this.changeFolder(item.Folder.Name)}}><img src={this.props.siteUrl+'/PublishingImages/documentIcon/folderIcon.png'} width="30px" style={{paddingRight:"10px"}}/> {item.Folder.Name}</div>
          );
        }
      });
      if(returnItem.length >0){
        return (
          <div style={{height:'408px', overflow:'hidden',overflowY:'auto'}}>{returnItem}</div>
        );
      }else{
        return(
          <div style={{height:'408px'}}>
            <div className={styles.centerDiv} style={{textAlign:'center'}}>
              <FontAwesomeIcon icon="file-alt" color="gainsboro" size="10x" />
              <br/><br/>
              <div style={{textAlign:'center', fontSize:'24px', color:'gainsboro'}}>No document currently.</div>
            </div>
          </div>
        );
      }
    }else{
      return(
        <div style={{height:'408px'}}>
          <div className={styles.centerDiv} style={{textAlign:'center'}}>
            <FontAwesomeIcon icon="file-alt" color="gainsboro" size="10x" />
            <br/><br/>
            <div style={{textAlign:'center', fontSize:'24px', color:'gainsboro'}}>That's seem no document on this site.</div>
          </div>
        </div>
      );
    }
  }

  private changeFolder(folder){
    let temp = this.state.previousFolder;
    temp.push(folder);
    this.setState({
      startFolder: folder,
      previousFolder: temp
    });
  }

  private backFolder(index,folder){
    let temp = [];
    this.state.previousFolder.forEach((fol,i)=>{
      if(i <= index){
        temp.push(fol)
      }
    });
    this.setState({
      startFolder: folder,
      previousFolder: temp
    });
  }

  private toggleUpload(){
    document.getElementById('backgroundModal').style.display = 'block';
    document.getElementById('mainModal').style.display = 'block';
    this.setState({
      modalContent:'Upload'
    });
  }

  private toggleDocumentModal(doc){
    document.getElementById('backgroundModal').style.display = 'block';
    document.getElementById('mainModal').style.display = 'block';
    this.setState({
      selectedDocument: doc,
      modalContent: 'Document'
    });
  }

  private renderEvents(){
    if(this.state.relatedEvent.length == 0){
      return(
        <div style={{height:'460px'}}>
          <div className={styles.centerDiv} style={{textAlign:'center'}}>
            <FontAwesomeIcon icon="calendar-alt" color="gainsboro" size="10x" />
            <br/><br/>
            <div style={{textAlign:'center', fontSize:'24px', color:'gainsboro'}}>No upcoming events.</div>
          </div>
        </div>
      );
    }else{

    }
  }

  private renderVideo(){
    return(
      <div style={{height:'460px'}}>
        <div className={styles.centerDiv} style={{textAlign:'center'}}>
          <FontAwesomeIcon icon="video" color="gainsboro" size="10x" />
          <br/><br/>
          <div style={{textAlign:'center', fontSize:'24px', color:'gainsboro'}}>No video on this site.</div>
        </div>
      </div>
    );
  }

  private selectedFile(e){
    e.preventDefault();
    let input = document.getElementById('inFile');
    input.click();
  }

  private getSelectedFile(e){
    e.preventDefault();
    let tempSelect = e.target.files;
    let tempNo;
    let lastTemp = this.state.fileSelect;
    for(var i = 0 ; i < tempSelect.length; i++){
      lastTemp.push(tempSelect[i]);
    }
    if(lastTemp.length==0){
      tempNo = ' no file select';
    }else{
      tempNo = lastTemp.length + ' file select';
    }
    this.setState({
      fileSelect: lastTemp,
      selectNo: tempNo
    });
  }

  
  private deleteSelectedFile(e, index){
    e.preventDefault();
    let tempSelect = this.state.fileSelect;
    let tempNo;
    for(var i = 0; i < tempSelect.length; i++){
      if(i == index){
        tempSelect.splice(i,1);
        if(tempSelect.length==0){
          tempNo = ' no file select';
          this.setState({
            fileSelect: tempSelect,
            selectNo: tempNo
          });
        }else{
          tempNo = tempSelect.length + ' file select';
          this.setState({
          fileSelect: tempSelect,
          selectNo: tempNo
        });
        }
      }
    }
  }

  private showFileSelected(){
    if(this.state.fileSelect.length != 0){
      console.log(this.state.fileSelect);
      let file = this.state.fileSelect.map((item,index)=>{
        return (
          <div className="row no-gutters" style={{padding:"5px 5px"}}>
            <div className="col no-gutters"><div><a style={{cursor:'pointer'}} onClick={(e)=>{this.deleteSelectedFile(e,index)}}><FontAwesomeIcon icon="minus" color="red"/></a> {item.name}</div></div>
          </div>
        );
      });
      return file;
    }
  }

  // private uploadFile(){
  //   this.state.fileSelect[0].blob
  //   pnp.sp.web.getFolderByServerRelativePath('/sites/HRInside/TemplateFolder/').files
  // }

  private renderModalContent(){
    if(this.state.modalContent == 'Document'){
      let create = this.state.selectedDocument.File.TimeCreated.split('T');
      return(
        <div style={{padding:'20px'}}>
          <div className="row" style={{margin:'0'}}>
            <img src={this.props.siteUrl+'/PublishingImages/documentIcon/'+this.state.selectedDocument.File_x0020_Type+'Icon.png'} width="50px" height="50px" style={{paddingRight:"10px"}}/>
            <div className="col" style={{padding:'0'}}>
              <text className={styles.modalTitle}> {this.state.selectedDocument.File.Name}</text><br/>
              Created By: {this.state.selectedDocument.Author.Title} | Created Date: {create[0]}
            </div>
            <div style={{cursor:'pointer'}} onClick={()=>{this.closeMainModal();}}>
              <FontAwesomeIcon icon="times" color="gainsboro" />
            </div>
          </div>
          <hr/>
          <div className="row no-gutters">
            <iframe src={this.state.selectedDocument.File.ServerRelativeUrl} style={{width:'100%',height:'65vh'}}></iframe>
          </div>
          <hr/>
          <div style={{textAlign:'right'}}>
            <button type="button" className="btn btn-secondary" onClick={()=>{this.closeMainModal();}}>Close</button>
          </div>
        </div>
      )
    }else if(this.state.modalContent == 'Video'){

    }else if(this.state.modalContent == 'Event'){

    }else if(this.state.modalContent == 'Upload'){
      let folder = this.state.documentItems.map((item)=>{
        if(item.FileRef.split('/')[item.FileRef.split('/').length-2] !== 'TemplateFolder'){
          if(item.File_x0020_Type == null){
            return(<option>{item.Folder.Name}</option>)
          }
        }
      });
      return(
        <div style={{padding:"20px"}}>
          <div className="row" style={{margin:'0'}}>
            <div className="col" style={{padding:'0'}}>
              <div style={{textAlign:'center', fontSize:'1.5em', fontWeight:800}}> {/* Header */}
                <b>Please insert this information</b>
              </div>
            </div>
            <div style={{cursor:'pointer'}} onClick={()=>{this.closeMainModal();}}>
              <FontAwesomeIcon icon="times" color="gainsboro" />
            </div>
          </div>
          <hr/>
          <div className="row no-gutters">
            <form style={{width:"100%", fontSize:'16px'}}>
              <div className="form-group row">
                <label className="col-sm-2 col-form-label" style={{fontWeight:800}}>Upload to <text style={{color:'red'}}>*</text></label>
                <div className="col-sm-10">
                  <select className="form-control" id="folderList" defaultValue="Documents">
                    <option value="Documents">Documents</option>
                    {folder}
                  </select>
                </div>
              </div>
              <div className="form-group row">
                <div className="col-sm-2" style={{fontWeight:800}}>Create new folder</div>
                <div className="col-sm-10">
                  <div className="form-check">
                    <input className="form-check-input" type="checkbox" id="folder" /> <br/>
                  </div>
                  <text style={{color:'grey'}}>It will create a new folder under the folder you selected above.</text>
                </div>
              </div>
              <div className="row" style={{paddingTop:'10px'}}>
                <div className="col">
                  <button type="button" className={styles.uploadImageButton} onMouseDown={(e)=>{this.selectedFile(e);}}>
                    <FontAwesomeIcon icon="plus" color="white" /> Upload Files
                    <input type="file" id="inFile" multiple style={{display:'none'}} onChange={(e)=>{this.getSelectedFile(e);}}/>
                  </button>
                  <text>{" "+this.state.selectNo}</text>
                </div>
              </div>
              <div className="row" style={{paddingTop:'10px'}}>
                <div className="col">
                  {this.showFileSelected()}
                </div>
              </div>
            </form>
          </div>
        </div>
      )
    }else{
      return(<div></div>);
    }
  }

  private closeMainModal(){
    document.getElementById('backgroundModal').style.display = 'none';
    document.getElementById('mainModal').style.display = 'none';
    this.setState({
      modalContent: ''
    });
  }

  private getDocuments(){
    let temp = window.location.href.split('?');
    let title = atob(temp[1]);
    console.log(title);
    
    pnp.sp.web.lists.getByTitle(title+'Folders').items.select(`File_x0020_Type,Modified,EditorId,Id,FileSystemObjectType,FileRef,Author/Title`).expand(`File,Folder,Author`).get().then((res)=>{
      console.log(res);
      
      this.setState({
        documentItems: res
      });


    });
  }

  private getVideo(){

  }

  private getRelatedEvents(thisMonth,thisYear){
    let startThis = thisYear+"-"+thisMonth+"-01T00%3a00%3a00Z";
    pnp.sp.web.lists.getByTitle('TemplateEvents').items.filter(`(EventDate ge datetime'${startThis}')`).top(3).get().then((res)=>{
      this.setState({
        relatedEvent: res
      });
    });
  }

  private getEvents(previousMonth,previousYear,nextMonth,nextYear,thisMonth,thisYear){
    if(thisMonth<10){
      thisMonth = "0"+thisMonth;
    }
    if(nextMonth<10){
      nextMonth = "0"+nextMonth;
    }
    if(previousMonth<10){
      previousMonth = "0"+previousMonth;
    }

    let startThis = thisYear+"-"+thisMonth+"-01T00%3a00%3a00Z";
    let nextStart = nextYear+"-"+nextMonth+"-01T00%3a00%3a00Z";
    let previousThis = previousYear+"-"+previousMonth+"-15T00%3a00%3a00Z";
    let endNext = nextYear+"-"+nextMonth+"-15T00%3a00%3a00Z";

    pnp.sp.web.lists.getByTitle('TemplateEvents').items.filter(`(EventDate ge datetime'${startThis}') and (EndDate lt datetime'${nextStart}')`).get().then((res)=>{
      pnp.sp.web.lists.getByTitle('TemplateEvents').items.filter(`(EventDate ge datetime'${nextStart}') and (EndDate lt datetime'${endNext}')`).get().then((nextRes)=>{ 
        pnp.sp.web.lists.getByTitle('TemplateEvents').items.filter(`(EventDate ge datetime'${previousThis}') and (EndDate lt datetime'${startThis}')`).get().then((nextPrev)=>{ 
          console.log(res,nextRes,nextPrev);
          
          this.setState({
            nextMonthEvents: nextRes,
            previousMonthEvents: nextPrev,
            thisMonthEvents: res
          });
        });
      });
    });
  }
}
