export interface IState{
    documentItems: any[];
    selectedDocument: any;
    startFolder: string;
    selectNo: string;
    fileSelect: any[];

    selectedEvent: any;
    nextMonthEvents: any[];
    previousMonthEvents: any[];
    thisMonthEvents: any[];
    relatedEvent: any[];

    thisMonth: number;
    previousMonth: number;
    nextMonth: number;
    thisYear: number;
    nextYear: number;
    previousYear: number;

    discussionItem: any[];
    previousFolder: any[];

    modalContent: string;

    postItem: any[],
    currentUser: any,
    allUser: any[],
    shareDescription: string,
    shareTitle: string,
    peopleSearch: any[],
    peopleSelect: any[]
}