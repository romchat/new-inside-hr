declare interface IHome1TemplateWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'Home1TemplateWebPartStrings' {
  const strings: IHome1TemplateWebPartStrings;
  export = strings;
}
