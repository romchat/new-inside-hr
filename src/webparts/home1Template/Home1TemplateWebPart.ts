import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-webpart-base';

import * as strings from 'Home1TemplateWebPartStrings';
import Home1Template from './components/Home1Template';
import { IHome1TemplateProps } from './components/IHome1TemplateProps';

export interface IHome1TemplateWebPartProps {
  description: string;
}

export default class Home1TemplateWebPart extends BaseClientSideWebPart<IHome1TemplateWebPartProps> {

  public render(): void {
    const element: React.ReactElement<IHome1TemplateProps > = React.createElement(
      Home1Template,
      {
        description: this.properties.description,
        siteUrl: this.context.pageContext.web.absoluteUrl,
        spHttpClient: this.context.spHttpClient
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
