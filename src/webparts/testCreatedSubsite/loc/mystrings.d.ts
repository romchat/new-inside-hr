declare interface ITestCreatedSubsiteWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'TestCreatedSubsiteWebPartStrings' {
  const strings: ITestCreatedSubsiteWebPartStrings;
  export = strings;
}
