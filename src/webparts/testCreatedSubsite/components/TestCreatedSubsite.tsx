import * as React from 'react';
import styles from './TestCreatedSubsite.module.scss';
import { ITestCreatedSubsiteProps } from './ITestCreatedSubsiteProps';
import { IState } from './IState';
import { escape } from '@microsoft/sp-lodash-subset';
import pnp from 'sp-pnp-js';

export default class TestCreatedSubsite extends React.Component<ITestCreatedSubsiteProps, IState> {
  constructor(props: ITestCreatedSubsiteProps, state: IState){
    super(props);
    this.state = {
      status: ''
    }
    let item = 'kemi';
    console.log(item.split(';'));
    
  }
  public render(): React.ReactElement<ITestCreatedSubsiteProps> {
    return (
      <div className={ styles.testCreatedSubsite }>
        <div className={ styles.container }>
          <div className={ styles.row }>
            <div className={ styles.column }>
              <span className={ styles.title }>Welcome to SharePoint!</span>
              <p className={ styles.subTitle }>Customize SharePoint experiences using Web Parts.</p>
              <p className={ styles.description }>{escape(this.props.description)}</p>
              <input type="text" id="title"></input>
              <input type="text" id="url"></input>
              <input type="text" id="description"></input>
              <a className={ styles.button } onClick={()=>{this.createSubsite()}}>
                <span className={ styles.label }>Create</span>
              </a>
              <br/><br/>
                {this.state.status}
            </div>
          </div>
        </div>
      </div>
    );
  }

  private createSubsite(){
    let title = document.getElementById('title') as HTMLInputElement;
    let url = document.getElementById('url') as HTMLInputElement;
    let desc = document.getElementById('description') as HTMLInputElement;
    this.setState({
      status: 'Waiting'
    });
    setTimeout(()=>{
      pnp.sp.web.lists.getByTitle('All Group').items.add({
        Title: title.value,
      });
    },3000);
  }
}
